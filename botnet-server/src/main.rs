use std::clone;
use std::collections::HashMap;
use std::error::Error;
use std::fs::{File, self};
use std::io::{Write, Read};

use async_std::io::{WriteExt, ReadExt};
use async_std::net::TcpStream;
use async_std::sync::{Mutex, Arc};
use async_std::{task, io, net::TcpListener};
use async_std::stream::StreamExt;
use uuid::{Uuid};

use serde::{Serialize, Deserialize};
use serde_json;
extern crate sysinfo;

use sysinfo::{System, SystemExt};

mod SystemInfo;

static mut VECTOR_UUIDS : Vec<Uuid> = Vec::new();
static mut VECTOR_SERIALIZED: Vec<String> = Vec::new();


fn get_system_informations() -> SystemInfo::SystemInfo{
    let mut sys = System::new_all();

    // First we update all information of our `System` struct.
    sys.refresh_all();

    let mut system_info_b = String::new();
    // Display system information:
    system_info_b.push_str(&format!("££System name: {};£", sys.name().unwrap().replace("//","£")));
    system_info_b.push_str(&format!("System kernel version: {};£", sys.kernel_version().unwrap().replace("//","£")));
    system_info_b.push_str(&format!("System OS version: {};£", sys.os_version().unwrap().replace("//","£")));
    system_info_b.push_str(&format!("System host name: {};££", sys.host_name().unwrap().replace("//","£")));

    /******/
    //println!("1) -> {}", String::from(system_info_b.clone()));
    /******/
    
    SystemInfo::SystemInfo {
        systemInfo : String::from(system_info_b),
    }
}

#[async_std::main]
async fn main() -> io::Result<()>{
    
    let lock_vec : Arc<Mutex<u8>> = Arc::new(Mutex::new(0));

    let connections: Vec<TcpStream> = vec![];
    let connections = Arc::new(Mutex::new(connections));
    
    let listener = TcpListener::bind("127.0.0.1:8080").await?;
    let mut incoming = listener.incoming();

    while let Some(stream) = incoming.next().await {
        let stream = stream?;
        let connections = connections.clone();
        let mut write_permission = connections.lock().await;
        write_permission.push(stream.clone());
        drop(write_permission);
        let lock_vec_ = Arc::clone(&lock_vec);
        task::spawn(on_connection(stream, connections, lock_vec_));
    }

    Ok(())
}

async fn on_connection(mut stream: TcpStream, connections: Arc<Mutex<Vec<TcpStream>>>, lock_vec_ : Arc<Mutex<u8>>) -> io::Result<()>{
    let addr = stream.peer_addr()?;

    let mut buffer = [0u8; 1024];
    loop {
        let len = stream.read(&mut buffer).await?;
        if len > 0 {
            let message_received = String::from_utf8_lossy(&buffer[..len]);
            //println!("{}", &message_received);

            //The message
            let to_split : Vec<&str> = message_received.as_ref().split(":").collect();

            //the code number
            let to_match = *to_split.get(0).unwrap();

            //the data
            let data = *to_split.get(1).unwrap();

            let lock_vec = Arc::clone(&lock_vec_);

            let mut uuid : Uuid = Uuid::new_v4();
            match to_match{

                "00001" => {
                    println!("the master is connected !");
                },

                "00002" => {
                    println!("ping {}", data);
                },

                _ => println!("code number not recognized")
            }
        } else {
            println!("Disconnected: {}", stream.peer_addr()?);
            let mut connections_guard = connections.lock().await;
            
            let client_index = connections_guard.iter().position(|x| 
                (*x).peer_addr().unwrap() == stream.peer_addr().unwrap()).unwrap();
            connections_guard.remove(client_index);

            break
        }
    }

    Ok(())
    
}

fn read_vec_from_file(filename: &str) -> (Vec<Uuid>, Vec<String>){

    let mut vec_uuid : Vec<Uuid> = Vec::new();
    let mut vec_serialized : Vec<String> = Vec::new();
    // Read the contents of the file as a string
    let mut file = fs::File::open(filename)
        .expect("Failed to open file");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Failed to read file");

    for s in contents.split("#") {
        // Check if the string slice can be parsed as a Uuid value
        let mut ss = s.split("@");
        //let uuid : Uuid;
        //let serialized_struct : String;

        if let Ok(uuid) = Uuid::parse_str(ss.next().unwrap()) {
            // If it can, push the Uuid value onto the vec_uuid vector
            vec_uuid.push(uuid);
        }

        let next = ss.next();

        if next == None {continue} else {vec_serialized.push(String::from(next.unwrap()))};
        
    }

    /*
    
    // Split the string on the comma character and convert the resulting
    // strings to integers
    let ids: Vec<&str> = contents
        .split(",")
        .map(|s| s.trim())
        .collect();
    /*
    let numbers: Vec<i32> = contents
        .split(",")
        .map(|s| s.trim())
        .map(|s| s.parse().expect("Failed to parse integer"))
        .collect();
    */
    
    for e in &ids{
        if let Ok(temp) = Uuid::parse_str(e.clone()) {
            vec_uuid.push(temp);
        }else{
            println!("an error has occured");
        }
    }
    */

    // Print the resulting vector
    //println!("{:?}", vec_uuid);
    (vec_uuid, vec_serialized)
}

// Define a function that takes a `Vec<i64>` and a file path as arguments
fn write_vec_to_file(vec_uuid: Vec<Uuid>, vec_serialized : Vec<String>, path: &str) {
    // Open the file at the specified path
    let mut file = match File::create(path) {
        Ok(file) => file,
        Err(err) => panic!("Error creating file: {}", err),
    };

    // Convert the `Vec<i64>` to a `String`
    let vec_string_uuid = vec_uuid.iter()
        .map(|n| n.to_string())
        .collect::<Vec<String>>()
        .join(",");

    // Write the `String` vec_uuid to the file
    match file.write_all(vec_string_uuid.as_bytes()) {
        Ok(_) => (),
        Err(err) => panic!("Error writing to file: {}", err),
    };
    
    match file.write_all(":".as_bytes()) {
        Ok(_) => (),
        Err(err) => panic!("Error writing to file: {}", err),
    };

    //println!("value : {:?}", vec_serialized);

    let vec_string_serialized = vec_serialized.join(",");

    //println!("to add after : {:?}", vec_string_serialized);

    // Write the `String` vec_serialized to the file
    match file.write_all(vec_string_serialized.as_bytes()) {
        Ok(_) => (),
        Err(err) => panic!("Error writing to file: {}", err),
    };

    match file.write_all("#".as_bytes()) {
        Ok(_) => (),
        Err(err) => panic!("Error writing to file: {}", err),
    };

}
